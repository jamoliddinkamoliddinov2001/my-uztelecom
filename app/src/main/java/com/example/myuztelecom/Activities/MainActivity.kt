package com.example.myuztelecom.Activities

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.baoyz.widget.PullRefreshLayout
import com.example.myuztelecom.FragmentsAll.HomePageFragments.HomePageFragment
import com.example.myuztelecom.FragmentsAll.SettingsFragment.SettingsFragment
import com.example.myuztelecom.Interfaces.ForCheckResults
import com.example.myuztelecom.R
import com.google.android.material.bottomappbar.BottomAppBar
import com.google.android.material.shape.CornerFamily
import com.google.android.material.shape.MaterialShapeDrawable
import kotlinx.android.synthetic.main.activity_main.floating_btn_home
import kotlinx.android.synthetic.main.testlayout.*
import java.util.*

class MainActivity : AppCompatActivity() {
    private lateinit var relayout: PullRefreshLayout

    override fun onResume() {
        bottom_nav_view.selectedItemId = 0
        super.onResume()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.testlayout)

        //refresh
        relayout = findViewById<PullRefreshLayout>(R.id.refreshlayout)

        getLanguage()
        val actionbar = supportActionBar
        actionbar?.title = resources.getString(R.string.app_name)

        bottom_nav_view.selectedItemId = 0

        val bottomnavview = findViewById<BottomAppBar>(R.id.bottomBar)
        val radius = resources.getDimension(R.dimen.radius_small)
        val bottomNavigationViewBackground = bottomnavview.background as MaterialShapeDrawable
        bottomNavigationViewBackground.shapeAppearanceModel =
            bottomNavigationViewBackground.shapeAppearanceModel.toBuilder()
                .setTopRightCorner(CornerFamily.ROUNDED, radius)
                .setTopLeftCorner(CornerFamily.ROUNDED, radius)
                .build()


        bottom_nav_view.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.menu_balans -> startPhoneActionIntent("*100#")
                R.id.menu_operator -> startPhoneActionIntent("1099")
                R.id.menu_settings -> if (ForCheckResults.getNavPagesResult()) {
                    //recreate()
                    replaceFragment(SettingsFragment())
                    //openLanguageDialog()
                }
            }
            true
        }

        floating_btn_home.setOnClickListener {
            bottom_nav_view.selectedItemId = 0
            ForCheckResults.checkNavPages(true)
            if (ForCheckResults.getFirstTimeResult()) {
                replaceFragment(HomePageFragment())
            }
        }
        supportFragmentManager.beginTransaction().replace(
            R.id.frame_for_fragments,
            HomePageFragment()
        ).commit()

        //for refresh layout
        relayout.setOnRefreshListener {
            // data yangilanishi kiritilishi kk bu yerga !!!!
            //handler orniga coroutine bolishi kk
            Handler().postDelayed({
                relayout.setRefreshing(false)
                Toast.makeText(
                    applicationContext,
                    getString(R.string.data_change),
                    Toast.LENGTH_SHORT
                ).show()
            }, 2000)
        }
    }

    private fun startPhoneActionIntent(phoneCode: String) {
        startActivity(
            Intent(
                Intent.ACTION_DIAL, Uri.fromParts(
                    "tel", phoneCode,
                    null
                )
            )
        )
    }

    private fun replaceFragment(fragment: Fragment) {
        Toast.makeText(applicationContext, "ketdi", Toast.LENGTH_SHORT).show()
        supportFragmentManager.beginTransaction()
            .setCustomAnimations(R.anim.enter_from_bottom, R.anim.exit_to_top)
            .replace(R.id.frame_for_fragments, fragment)
            .commit()
    }
    fun setLanguage(lang: String) {
        val locale = Locale(lang)
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        Log.d("TAGLANG", lang)
        baseContext.resources.updateConfiguration(config, baseContext.resources.displayMetrics)

        val editor = getSharedPreferences("language_settings", Context.MODE_PRIVATE).edit()
        editor.putString("language", lang)
        editor.apply()
    }

    fun getLanguage() {
        val language_prefs = getSharedPreferences("language_settings", Context.MODE_PRIVATE)
        val language = language_prefs.getString("language", "en") as String
        setLanguage(language)
    }
}