package com.example.myuztelecom.Interfaces

interface ForCheckResults {
    companion object {
        var isFirstTimeOpen = true
        var isNavPagesOpen = true
        var viewPagerPosition = 0

        fun setFirstTime(result: Boolean) {
            isFirstTimeOpen = result
        }

        fun getFirstTimeResult(): Boolean {
            return isFirstTimeOpen
        }

        fun checkNavPages(result: Boolean) {
            isNavPagesOpen = result
        }

        fun getNavPagesResult(): Boolean {
            return isNavPagesOpen
        }

        fun setViewPagerPos(result: Int) {
            viewPagerPosition = result
        }

        fun getViewPagerPos(): Int {
            return viewPagerPosition
        }
    }

}