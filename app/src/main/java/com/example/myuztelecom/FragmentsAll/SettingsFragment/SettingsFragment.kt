package com.example.myuztelecom.FragmentsAll.SettingsFragment

import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat.recreate
import androidx.fragment.app.Fragment
import com.example.myuztelecom.Interfaces.ForCheckResults
import com.example.myuztelecom.FragmentsAll.HomePageFragments.HomePageFragment
import com.example.myuztelecom.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_settings.*
import kotlinx.android.synthetic.main.fragment_settings.view.*
import kotlinx.android.synthetic.main.testlayout.*
import java.util.*

@Suppress("DEPRECATED_IDENTITY_EQUALS", "CAST_NEVER_SUCCEEDS")
class SettingsFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        /**bottom navifgation clickda oyna faqat bir marta ochilishi un */
        ForCheckResults.checkNavPages(false)
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && event.action === KeyEvent.ACTION_UP) {
                replaceFragment(HomePageFragment())
                return@OnKeyListener true
            }
            false
        })
        view.radio_group.check(getLangId())
        view.btn_save_settings.setOnClickListener {
            recreate(requireActivity())
        }

        view.radio_group.setOnCheckedChangeListener { group, checkedId ->
            Log.d("TAGID", "$checkedId")
            if (checkedId == R.id.radio_btn_uzb) {
                setLanguage("en")
            } else if (checkedId == R.id.radio_btn_rus) {
                setLanguage("ru")
            }
            setLangId(checkedId)
            Log.d("check_id", "$checkedId")
        }
        super.onViewCreated(view, savedInstanceState)
    }

    private fun replaceFragment(fragment: Fragment) {
        requireActivity().bottom_nav_view.selectedItemId = 0
        fragmentManager!!.beginTransaction()
            .setCustomAnimations(
                R.anim.enter_from_left,
                R.anim.exit_to_right
            )
            .replace(R.id.frame_for_fragments, fragment)
            .commit()
        onDestroy()
    }

    override fun onDestroy() {
        ForCheckResults.checkNavPages(true)
        super.onDestroy()
    }

    fun openLanguageDialog() {
        val languages = arrayOf("English", "Русский", "O'zbek tili")
        val builder = AlertDialog.Builder(requireContext())
            .setTitle(R.string.tilni_tanlang)
            .setSingleChoiceItems(languages, getLangId()) { dialog, i ->
                if (i == 0) {
                    setLanguage("en")
                    recreate(requireActivity())
                } else if (i == 1) {
                    setLanguage("ru")
                    recreate(requireActivity())
                }
                setLangId(i)
                Log.d("check_id", "$i")
                dialog.dismiss()
            }
        val dialog = builder.create()
        dialog.show()
    }

    fun setLanguage(lang: String) {
        val locale = Locale(lang)
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        Log.d("TAGLANG", lang)
        requireContext().resources.updateConfiguration(
            config,
            requireContext().resources.displayMetrics
        )

        val editor =
            requireActivity().getSharedPreferences("language_settings", Context.MODE_PRIVATE).edit()
        editor.putString("language", lang)
        editor.apply()
    }
    fun setLangId(id: Int) {
        val editor =
            requireActivity().getSharedPreferences("language_settings", Context.MODE_PRIVATE).edit()
        editor.putInt("lang_id", id)
        editor.apply()
    }

    fun getLangId(): Int {
        val language_prefs =
            requireActivity().getSharedPreferences("language_settings", Context.MODE_PRIVATE)
        return language_prefs.getInt("lang_id", R.id.radio_btn_uzb)
    }
}