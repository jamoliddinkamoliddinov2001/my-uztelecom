package com.example.myuztelecom.FragmentsAll.SMSFragments.ForTabs

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.concatexpandablerecyclerview.GroupItem
import com.example.concatexpandablerecyclerview.RecyclerViewConcatAdapter
import com.example.myuztelecom.R
import kotlinx.android.synthetic.main.fragment_kunlik_s_m_slar.*
import kotlinx.android.synthetic.main.fragment_oylik_s_m_slar.*

class OylikSMSlarFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_oylik_s_m_slar, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val adapterList= arrayListOf<RecyclerViewConcatAdapter>()
        val groupList=createItems()
        for (item in groupList){
            val adapter= RecyclerViewConcatAdapter(item,requireContext())
            adapterList.add(adapter)
        }
        val concatcConfig= ConcatAdapter.Config.Builder().setIsolateViewTypes(false).build()
        val adapter= ConcatAdapter(concatcConfig,adapterList)
        concat_rv_sms_oylik.layoutManager= LinearLayoutManager(requireContext())
        concat_rv_sms_oylik.adapter=adapter
        super.onViewCreated(view, savedInstanceState)
    }
    fun createItems():List<GroupItem>{
        val titles = arrayListOf<String>(
            "SMS 10",
            "SMS 20",
            "SMS 30",
            "SMS 40",
            "SMS 50",
            "SMS 60",
            "SMS 70",
            "SMS 80",
            "SMS 90",
            "SMS 100"
        )
        val groupList= arrayListOf<GroupItem>()
        for(i in 1..titles.size){
            val textHeader="${i*10}"
            val header= GroupItem.HeaderItem(textHeader,R.color.uzmobile,titles[i-1])
            val groupItem= GroupItem(header, GroupItem.SubItem("Sotib Olish"))
            groupList.add(groupItem)
        }
        return groupList
    }
}