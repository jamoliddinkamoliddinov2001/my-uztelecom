package com.example.myuztelecom.FragmentsAll.XizmatlarFragments

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import com.baoyz.widget.PullRefreshLayout
import com.example.concatexpandablerecyclerview.GroupItem
import com.example.concatexpandablerecyclerview.RecyclerViewConcatAdapter
import com.example.myuztelecom.FragmentsAll.HomePageFragments.HomePageFragment
import com.example.myuztelecom.FragmentsAll.USSDFragments.USSDFragment
import com.example.myuztelecom.R
import kotlinx.android.synthetic.main.fragment_u_s_s_d.*
import kotlinx.android.synthetic.main.fragment_u_s_s_d.view.*
import kotlinx.android.synthetic.main.fragment_xizmatlar.*
import kotlinx.android.synthetic.main.fragment_xizmatlar.view.*

class XizmatlarFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_xizmatlar, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val adapterList = arrayListOf<RecyclerViewConcatAdapter>()
        val groupList = createItems()
        for (item in groupList) {
            val adapter = RecyclerViewConcatAdapter(item, requireContext())
            adapterList.add(adapter)
        }
        val concatcConfig = ConcatAdapter.Config.Builder().setIsolateViewTypes(false).build()
        val adapter = ConcatAdapter(concatcConfig, adapterList)
        concat_rv_xizmatlar.layoutManager = LinearLayoutManager(requireContext())
        concat_rv_xizmatlar.adapter = adapter
        view.btn_back_xizmatlar.setOnClickListener {
            val manager = fragmentManager
            val ussdFragment = USSDFragment()
            manager!!.beginTransaction().remove(ussdFragment).commit()
            Log.d("TAGREMOVE", "o'chdi")
        }
        /**<Action OnBack>*/
        view.btn_back_xizmatlar.setOnClickListener {
            replaceFragment(HomePageFragment())
        }
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && event.action === KeyEvent.ACTION_UP) {
                replaceFragment(HomePageFragment())
                return@OnKeyListener true
            }
            false
        })
        /**<Action OnBack/>*/

        /***refresh layout*/
        val relayout = view.findViewById<PullRefreshLayout>(R.id.refreshlayout1)
        relayout.setOnRefreshListener {
            // data yangilanishi kiritilishi kk bu yerga !!!!
            //handler orniga coroutine bolishi kk
            Handler().postDelayed({
                relayout.setRefreshing(false)
                Toast.makeText(
                    requireContext(),
                    "shuffled",
                    Toast.LENGTH_SHORT
                ).show()
            }, 1000)
        }

        super.onViewCreated(view, savedInstanceState)
    }
    private fun replaceFragment(fragment: Fragment) {
        fragmentManager!!.beginTransaction().setCustomAnimations(
            R.anim.enter_from_right, R.anim.exit_to_left,
            R.anim.enter_from_right, R.anim.exit_to_left
        )
            .replace(R.id.frame_for_fragments, fragment)
            .commit()
    }

    fun createItems(): List<GroupItem> {
        val groupList = arrayListOf<GroupItem>()

        val textHeader = ""
        val titles = arrayListOf<String>(
            "LTE 4D",
            "Men kimman",
            "Yashirin qo'ng'iroq",
            "Kunlik Vip",
            "Tungi qo'ng'iroq",
            "Ojidanie Xizmati",
            "Oila uchun xizmati",
            "Internet non-stop"
        )
        for (i in titles.indices) {
            val header = GroupItem.HeaderItem(textHeader, R.drawable.uztelecom, titles[i])
            val groupItem = GroupItem(header, GroupItem.SubItem("Ulanish"))
            groupList.add(groupItem)
        }
        return groupList
    }
}