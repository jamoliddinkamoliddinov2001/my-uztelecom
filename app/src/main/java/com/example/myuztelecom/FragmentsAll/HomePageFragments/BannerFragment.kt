package com.example.myuztelecom.FragmentsAll.HomePageFragments

import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.myuztelecom.Adapters.ForViewPagerFlipper.IntroItem
import com.example.myuztelecom.R
import kotlinx.android.synthetic.main.fragment_banner.*


private const val BANNER_POS = "page_position"

@Suppress("DEPRECATED_IDENTITY_EQUALS")
class BannerFragment : Fragment() {
    lateinit var list: MutableList<IntroItem>
    private var param1: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getInt(BANNER_POS)
            list = ArrayList()
            list.apply {
                add(IntroItem("Oddiy 10", 10, 10, 10, 10000))
                add(IntroItem("Flash", 1000, 2000, 8000, 70000))
                add(IntroItem("Street", 750, 750, 6500, 39900))
                add(IntroItem("Royal", 10000, 10000, 10000, 100000))
                add(IntroItem("Onlime", 1000, 1000, 10000, 49900))
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d("POST", "$param1")
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_banner, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val tvName = flipper_include.findViewById<TextView>(R.id.tv_tarif_name)
        val tvMin = flipper_include.findViewById<TextView>(R.id.tv_min)
        val tvMb = flipper_include.findViewById<TextView>(R.id.tv_mb)
        val tvSMS = flipper_include.findViewById<TextView>(R.id.tv_sms)
        val tvPriceIncluded = flipper_include.findViewById<TextView>(R.id.tv_tolov)

        val tvTarifDesc = flipper_include.findViewById<TextView>(R.id.tarif_desc)

        val btnBatafsil = flipper_include.findViewById<Button>(R.id.btn_batafsil)
        val btnUlanish = flipper_include.findViewById<Button>(R.id.btn_ulanish)

        val btnBack = view.findViewById<ImageButton>(R.id.btn_back_banner)

        btnBack.setOnClickListener {
            fragmentManager!!.beginTransaction()
                .setCustomAnimations(
                    R.anim.enter_from_left,
                    R.anim.exit_to_right
                )
                .replace(R.id.frame_for_fragments, HomePageFragment())
                .commit()
        }

        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && event.action === KeyEvent.ACTION_UP) {
                fragmentManager!!.beginTransaction()
                    .setCustomAnimations(
                        R.anim.enter_from_left,
                        R.anim.exit_to_right
                    )
                    .replace(R.id.frame_for_fragments, HomePageFragment())
                    .commit()
                return@OnKeyListener true
            }
            false
        })
        tv_tarif_name_banner.text = list[param1].tarifName
        tvName.text = list[param1].tarifName
        tvMin.text = list[param1].min.toString()
        tvSMS.text = list[param1].sms.toString()
        tvMb.text = list[param1].mb.toString()
        tv_tolov_banner.text = list[param1].tolov.toString()
        tvPriceIncluded.text = list[param1].tolov.toString()

        super.onViewCreated(view, savedInstanceState)

    }
}