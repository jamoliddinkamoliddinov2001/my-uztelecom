package com.example.myuztelecom.FragmentsAll.DaqiqalarFragments

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.myuztelecom.Adapters.ForTabLayout.TabAdapter
import com.example.myuztelecom.FragmentsAll.DaqiqalarFragments.ForTabs.HaftalikMinFragment
import com.example.myuztelecom.FragmentsAll.DaqiqalarFragments.ForTabs.KunlikMinFragment
import com.example.myuztelecom.FragmentsAll.DaqiqalarFragments.ForTabs.OylikMinFragment
import com.example.myuztelecom.FragmentsAll.DaqiqalarFragments.ForTabs.YillikMinFragment
import com.example.myuztelecom.FragmentsAll.HomePageFragments.HomePageFragment
import com.example.myuztelecom.R
import kotlinx.android.synthetic.main.fragment_daqiqalar.*
import kotlinx.android.synthetic.main.fragment_daqiqalar.view.*
import kotlinx.android.synthetic.main.fragment_internet.*
import kotlinx.android.synthetic.main.fragment_u_s_s_d.view.*

class DaqiqalarFragment : Fragment() {
    private val listFragments: MutableList<Fragment> = ArrayList()
    private val titles: MutableList<String> = ArrayList()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        listFragments.apply {
            add(KunlikMinFragment())
            add(HaftalikMinFragment())
            add(OylikMinFragment())
            add(YillikMinFragment())
        }
        titles.apply {
            add("Kunlik")
            add("Haftalik")
            add("Oylik")
            add("Yillik")
        }
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_daqiqalar, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val tabAdapter = TabAdapter(fragmentManager!!, listFragments, titles)
        view_pager_min.adapter = tabAdapter
        min_tab_layout.setupWithViewPager(view_pager_min)
        setMarginTabItems()
        /**<Action OnBack>*/
        view.btn_back_min.setOnClickListener {
            replaceFragment(HomePageFragment())
        }
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && event.action === KeyEvent.ACTION_UP) {
                replaceFragment(HomePageFragment())
                return@OnKeyListener true
            }
            false
        })
        /**<Action OnBack/>*/
        super.onViewCreated(view, savedInstanceState)
    }
    private fun replaceFragment(fragment: Fragment) {
        fragmentManager!!.beginTransaction().setCustomAnimations(
            R.anim.enter_from_right, R.anim.exit_to_left,
            R.anim.enter_from_right, R.anim.exit_to_left
        )
            .replace(R.id.frame_for_fragments, fragment)
            .commit()
    }
    fun setMarginTabItems() {
        for (i in 0 until min_tab_layout.tabCount) {
            val tabItem = (min_tab_layout.getChildAt(0) as ViewGroup).getChildAt(i)
            val params = tabItem.layoutParams as ViewGroup.MarginLayoutParams
            params.setMargins(25, 0, 25, 0)
            params.width = 270
            params.height = 100
        }
    }
}