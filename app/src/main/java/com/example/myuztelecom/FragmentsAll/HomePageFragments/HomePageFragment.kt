package com.example.myuztelecom.FragmentsAll.HomePageFragments

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.example.myuztelecom.Adapters.ForViewPagerFlipper.IntroAdapter
import com.example.myuztelecom.Adapters.ForViewPagerFlipper.IntroItem
import com.example.myuztelecom.Adapters.ForViewPagerFlipper.OnItemClickedListener
import com.example.myuztelecom.FragmentsAll.DaqiqalarFragments.DaqiqalarFragment
import com.example.myuztelecom.FragmentsAll.InternetFragments.InternetFragment
import com.example.myuztelecom.FragmentsAll.SMSFragments.SMSPaketlarFragment
import com.example.myuztelecom.FragmentsAll.TariflarFragments.TariflarFragment
import com.example.myuztelecom.FragmentsAll.USSDFragments.USSDFragment
import com.example.myuztelecom.FragmentsAll.XizmatlarFragments.XizmatlarFragment
import com.example.myuztelecom.Interfaces.ForCheckResults
import com.example.myuztelecom.R
import com.example.navigationdrwercustom.DrawerAdapter
import com.example.navigationdrwercustom.DrawerItem
import com.example.navigationdrwercustom.SimpleItem
import com.yarolegovich.slidingrootnav.SlidingRootNav
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder
import kotlinx.android.synthetic.main.fragment_home_page.*

@Suppress("UNCHECKED_CAST")

class HomePageFragment : Fragment(), DrawerAdapter.OnItemSelectedListener{
    /**<For Toolbar>*/
    private lateinit var screenTitles: Array<String>
    private lateinit var screenDrawables: Array<Drawable?>

    private var slidingRootNav: SlidingRootNav? = null

    companion object {
        private const val POS_CLOSE = 0
        private const val POS_MAIN = 1
        private const val POS_ABOUT = 2
        private const val POS_OUR_APPS = 3
        private const val POS_SHARE_APP = 4
        private const val POS_LOG_OUT = 5
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main_toolbar_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    /**<For Toolbar/>*/

    lateinit var introAdapter: IntroAdapter
    lateinit var list: MutableList<IntroItem>
    lateinit var indicatorContainer: LinearLayout
    lateinit var viewPagerCountDownTimer: CountDownTimer
    lateinit var toolbar: Toolbar
    var isTimerActive: Boolean = true

    override fun onResume() {
        view_pager_flipper.currentItem = ForCheckResults.getViewPagerPos()
        isTimerActive = true
        viewPagerCountDownTimer.start()
        ForCheckResults.setFirstTime(false)
        slidingRootNav?.isMenuLocked=false
        super.onResume()
    }

    override fun onPause() {
        slidingRootNav?.isMenuLocked=true
        ForCheckResults.setFirstTime(true)
        ForCheckResults.setViewPagerPos(view_pager_flipper.currentItem)
        viewPagerCountDownTimer.onFinish()
        viewPagerCountDownTimer.cancel()

        super.onPause()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        list = ArrayList<IntroItem>()
        Log.d("TAGhomekel", "keldi")
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_page, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        /**<Toolbar uchun>*/
        toolbar = view.findViewById<Toolbar>(R.id.tool_bar)
        toolbar.title = ""
        slidingRootNav = SlidingRootNavBuilder(requireActivity()).withDragDistance(180)
            .withRootViewScale(0.75f)
            .withRootViewElevation(25)
            .withToolbarMenuToggle(toolbar)
            .withMenuOpened(false)
            .withContentClickableWhenMenuOpened(false)
            .withSavedState(savedInstanceState)
            .withMenuLayout(R.layout.nav_drawer_layout)
            .inject()

        screenDrawables = loadScreenIcons()
        screenTitles = loadScreenTitles()

        val adapter = DrawerAdapter(
            listOf(
                createItemFor(HomePageFragment.POS_CLOSE),
                createItemFor(HomePageFragment.POS_MAIN),
                createItemFor(HomePageFragment.POS_ABOUT),
                createItemFor(HomePageFragment.POS_OUR_APPS),
                createItemFor(HomePageFragment.POS_SHARE_APP),
                createItemFor(HomePageFragment.POS_LOG_OUT)
            ) as List<DrawerItem<DrawerAdapter.viewHolder>>
        )
        adapter.setListener(this)
        val rv = requireActivity().findViewById<RecyclerView>(R.id.drawer_list)
        rv.layoutManager = LinearLayoutManager(requireContext())
        rv.adapter = adapter
        //default value
        adapter.setSelected(1)
        /**<Toolbar uchun/>*/


        /**<Reklamasi oynasi un>*/
        setIntroAdapter()
        setupIndicator()

        view_pager_flipper.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                setCurrentIndicatorActive(position)
            }
        })
        indicatorsSetonClickListener()
        setCurrentIndicatorActive(0)
        // Log.d("POSN", " ${ view_pager_flipper.currentItem}")

        introAdapter.setOnItemClicked(object : OnItemClickedListener {
            override fun onItemClicked(position: Int) {

                viewPagerCountDownTimer.onFinish()
                viewPagerCountDownTimer.cancel()
                slidingRootNav?.isMenuLocked=true
                isTimerActive = false
                val bannerFragment = BannerFragment()
                val bundle = Bundle()
                bundle.putInt("page_position", position)
                bannerFragment.arguments = bundle

                fragmentManager!!.beginTransaction()
                    .setCustomAnimations(
                        R.anim.enter_from_right, R.anim.exit_to_left,
                        R.anim.enter_from_right, R.anim.exit_to_left
                    )
                    .replace(R.id.frame_for_fragments, bannerFragment)
                    .commit()
            }
        })

        viewPagerCountDownTimer = object : CountDownTimer(Long.MAX_VALUE, 4000) {
            override fun onTick(p0: Long) {
//                Log.d("POSN", " ${view_pager_flipper.currentItem}+ ${p0/1000} ")
                if (isTimerActive) {
                    if (view_pager_flipper.currentItem == introAdapter.itemCount - 1)
                        view_pager_flipper.currentItem = 0
                    else
                        view_pager_flipper.currentItem++
                } else cancel()
            }

            override fun onFinish() {
            }
        }
        /**<Reklamasi oynasi un/>*/


        /**<GridItem lar uchun>*/
        btn_ussd.setOnClickListener {
            replaceFragment(USSDFragment())
        }
        btn_tarif.setOnClickListener {
            replaceFragment(TariflarFragment())
        }
        btn_xizmatlar.setOnClickListener {
            replaceFragment(XizmatlarFragment())
        }
        btn_daqiqa.setOnClickListener {
            replaceFragment(DaqiqalarFragment())
        }
        btn_internet.setOnClickListener {
            replaceFragment(InternetFragment())
        }
        btn_sms.setOnClickListener {
            replaceFragment(SMSPaketlarFragment())
        }
        /**<GridItem lar uchun/>*/


    }

    private fun setIntroAdapter() {
        list.apply {
            add(IntroItem("Oddiy 10", 10, 10, 10, 10000))
            add(IntroItem("Flash", 1000, 2000, 8000, 70000))
            add(IntroItem("Street", 750, 750, 6500, 39900))
            add(IntroItem("Royal", 10000, 10000, 10000, 100000))
            add(IntroItem("Onlime", 1000, 1000, 10000, 49900))
        }
        introAdapter = IntroAdapter(list)
        view_pager_flipper.adapter = introAdapter
    }

    private fun setupIndicator() {
        indicatorContainer = view!!.findViewById(R.id.indicator_container)
        val indicators = arrayOfNulls<ImageView>(introAdapter.itemCount)
        val layoutParams: LinearLayout.LayoutParams =
            LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        layoutParams.setMargins(10, 0, 10, 0)
        for (i in indicators.indices) {
            indicators[i] = ImageView(requireContext())
            indicators[i]?.setImageDrawable(
                ContextCompat.getDrawable(
                    requireContext(),
                    R.drawable.indicator_inactive
                )
            )
            indicators[i]?.layoutParams = layoutParams
            indicatorContainer.addView(indicators[i])
        }
    }

    private fun setCurrentIndicatorActive(position: Int) {
        val childCount = indicatorContainer.childCount
        for (i in 0 until childCount) {
            val indicator = indicatorContainer.getChildAt(i) as ImageView
            if (position == i) {
                Log.d("PPP", "$position $i")
                indicator.setImageDrawable(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.indicator_active
                    )
                )
                Log.d("PPP", "$position $i")
            } else {
                indicator.setImageDrawable(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.indicator_inactive
                    )
                )
            }
        }
    }

    private fun indicatorsSetonClickListener() {
        val indicatorItem1 = indicatorContainer.getChildAt(0) as ImageView
        val indicatorItem2 = indicatorContainer.getChildAt(1) as ImageView
        val indicatorItem3 = indicatorContainer.getChildAt(2) as ImageView
        indicatorItem1.setOnClickListener {
            viewPagerCountDownTimer.cancel()
            viewPagerCountDownTimer.start()
            view_pager_flipper.currentItem = 0
        }
        indicatorItem2.setOnClickListener {
            view_pager_flipper.currentItem = 1
        }
        indicatorItem3.setOnClickListener {
            view_pager_flipper.currentItem = 2
        }
    }
    private fun replaceFragment(fragment: Fragment) {
        slidingRootNav?.isMenuLocked=true
        fragmentManager!!.beginTransaction().setCustomAnimations(
            R.anim.enter_from_left, R.anim.exit_to_left,
            R.anim.enter_from_left, R.anim.exit_to_left
        )
            .replace(R.id.frame_for_fragments, fragment)
            .commit()
    }

    /**<for toolbar>*/

    private fun loadScreenTitles(): Array<String> {
        return resources.getStringArray(R.array.screenTitles)
    }

    private fun loadScreenIcons(): Array<Drawable?> {
        val typedArray = resources.obtainTypedArray(R.array.screenIcons)
        val icons = arrayOfNulls<Drawable>(typedArray.length())
        for (i in 0 until typedArray.length()) {
            val id = typedArray.getResourceId(i, 0)
            if (id != 0) {
                icons[i] = ContextCompat.getDrawable(requireContext(), id)
            }
        }
        typedArray.recycle()
        return icons
    }

    @ColorInt
    private fun color(@ColorRes rec: Int): Int {
        return ContextCompat.getColor(requireContext(), rec)
    }

    private fun createItemFor(position: Int): DrawerItem<*> {
        return SimpleItem(screenDrawables[position]!!, screenTitles[position])
            .withNormalIconTint(color(R.color.uzmobile))
            .withNormalTextTint(color(R.color.black))
            .withSelectedIconTint(color(R.color.uzmobile3))
            .withSelectedTextTint(color(R.color.uzmobile3))
    }

    override fun onNavDrawerItemSelected(position: Int) {
        when (position) {
            HomePageFragment.POS_MAIN -> Toast.makeText(
                requireContext(),
                "Asosiy menu",
                Toast.LENGTH_SHORT
            ).show()
            HomePageFragment.POS_ABOUT -> Toast.makeText(
                requireContext(),
                "Biz haqimizda",
                Toast.LENGTH_SHORT
            ).show()
            HomePageFragment.POS_OUR_APPS -> Toast.makeText(
                requireContext(),
                "Bizning dasturlar",
                Toast.LENGTH_SHORT
            ).show()
            HomePageFragment.POS_SHARE_APP -> Toast.makeText(
                requireContext(),
                "Ulashish",
                Toast.LENGTH_SHORT
            ).show()
            HomePageFragment.POS_LOG_OUT -> Toast.makeText(
                requireContext(),
                "Chiqish",
                Toast.LENGTH_SHORT
            ).show()
            HomePageFragment.POS_CLOSE -> {
                Toast.makeText(requireContext(), "Butunlay Chiqish", Toast.LENGTH_SHORT).show()
                requireActivity().finishAffinity()
            }
        }
        slidingRootNav!!.closeMenu()
    }
    /**<for toolbar/>*/
}


