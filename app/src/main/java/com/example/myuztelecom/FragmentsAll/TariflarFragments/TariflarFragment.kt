package com.example.myuztelecom.FragmentsAll.TariflarFragments

import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.concatexpandablerecyclerview.GroupItem
import com.example.concatexpandablerecyclerview.RecyclerViewConcatAdapter
import com.example.myuztelecom.FragmentsAll.HomePageFragments.HomePageFragment
import com.example.myuztelecom.FragmentsAll.USSDFragments.USSDFragment
import com.example.myuztelecom.R
import kotlinx.android.synthetic.main.fragment_banner.view.*
import kotlinx.android.synthetic.main.fragment_tariflar.*
import kotlinx.android.synthetic.main.fragment_tariflar.view.*
import kotlinx.android.synthetic.main.fragment_tariflar.view.btn_back_tariflar
import kotlinx.android.synthetic.main.fragment_u_s_s_d.*
import kotlinx.android.synthetic.main.fragment_u_s_s_d.view.*

class TariflarFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tariflar, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val adapterList= arrayListOf<RecyclerViewConcatAdapter>()
        val groupList=createItems()
        for (item in groupList){
            val adapter= RecyclerViewConcatAdapter(item,requireContext())
            adapterList.add(adapter)
        }
        val concatcConfig= ConcatAdapter.Config.Builder().setIsolateViewTypes(false).build()
        val adapter= ConcatAdapter(concatcConfig,adapterList)
        concat_rv_tariflar.layoutManager= LinearLayoutManager(requireContext())
        concat_rv_tariflar.adapter=adapter

        /**<Action OnBack>*/
        view.btn_back_tariflar.setOnClickListener {
            replaceFragment(HomePageFragment())
        }
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && event.action === KeyEvent.ACTION_UP) {
                replaceFragment(HomePageFragment())
                return@OnKeyListener true
            }
            false
        })
        /**<Action OnBack/>*/
        super.onViewCreated(view, savedInstanceState)
    }
    private fun replaceFragment(fragment: Fragment) {
        fragmentManager!!.beginTransaction().setCustomAnimations(
            R.anim.enter_from_right, R.anim.exit_to_left
        )
            .replace(R.id.frame_for_fragments, fragment)
            .commit()
    }
    fun createItems():List<GroupItem>{
        val titles = arrayListOf<String>(
            "Oddiy 10",
            "Delovoy",
            "Comfort",
            "Bolajon",
            "Yoshlar",
            "Traffic",
            "Traffic",
            "Silver",
            "Street",
            "Onlime",
            "Royal"
        )
        val groupList= arrayListOf<GroupItem>()
        for(i in titles.indices){
            val textHeader=""
            val header= GroupItem.HeaderItem(textHeader,R.drawable.uztelecom,titles[i])
            val groupItem= GroupItem(header, GroupItem.SubItem("Ulanish"))
            groupList.add(groupItem)
        }
        return groupList
    }
}