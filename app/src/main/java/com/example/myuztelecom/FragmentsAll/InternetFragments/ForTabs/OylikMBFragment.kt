package com.example.myuztelecom.FragmentsAll.InternetFragments.ForTabs

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.concatexpandablerecyclerview.GroupItem
import com.example.concatexpandablerecyclerview.RecyclerViewConcatAdapter
import com.example.myuztelecom.R
import kotlinx.android.synthetic.main.fragment_haftalik_m_b.*
import kotlinx.android.synthetic.main.fragment_oylik_m_b.*

class OylikMBFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_oylik_m_b, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val adapterList= arrayListOf<RecyclerViewConcatAdapter>()
        val groupList=createItems()
        for (item in groupList){
            val adapter= RecyclerViewConcatAdapter(item,requireContext())
            adapterList.add(adapter)
        }
        val concatcConfig= ConcatAdapter.Config.Builder().setIsolateViewTypes(false).build()
        val adapter= ConcatAdapter(concatcConfig,adapterList)

        concat_rv_mb_oylik.layoutManager= LinearLayoutManager(requireContext())
        concat_rv_mb_oylik.adapter=adapter
        super.onViewCreated(view, savedInstanceState)
    }
    fun createItems():List<GroupItem>{
        val titles = arrayListOf<String>(
            "50 MB",
            "100 MB",
            "500 MB",
            "1500 MB",
            "3000 MB",
            "6000 MB",
            "12000 MB",
            "50000 MB",
            "750000 MB"
        )
        val headerTitles = arrayListOf<String>(
            "50",
            "100",
            "500",
            "1500",
            "3000",
            "6000",
            "12000",
            "50000",
            "750000"
        )
        val groupList= arrayListOf<GroupItem>()
        for(i in titles.indices){
            val header= GroupItem.HeaderItem(headerTitles[i],R.color.uzmobile,titles[i])
            val groupItem= GroupItem(header, GroupItem.SubItem("Sotib Olish"))
            groupList.add(groupItem)
        }
        return groupList
    }
}