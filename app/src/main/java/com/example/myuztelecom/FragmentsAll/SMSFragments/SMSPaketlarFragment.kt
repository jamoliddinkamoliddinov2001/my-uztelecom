package com.example.myuztelecom.FragmentsAll.SMSFragments

import android.os.Bundle
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.myuztelecom.Adapters.ForTabLayout.TabAdapter
import com.example.myuztelecom.FragmentsAll.HomePageFragments.HomePageFragment
import com.example.myuztelecom.FragmentsAll.SMSFragments.ForTabs.HaftalikSMSlarFragment
import com.example.myuztelecom.FragmentsAll.SMSFragments.ForTabs.KunlikSMSlarFragment
import com.example.myuztelecom.FragmentsAll.SMSFragments.ForTabs.OylikSMSlarFragment
import com.example.myuztelecom.FragmentsAll.SMSFragments.ForTabs.XalqaroSMSlarFragment
import com.example.myuztelecom.R
import kotlinx.android.synthetic.main.fragment_s_m_s_paketlar.*
import kotlinx.android.synthetic.main.fragment_s_m_s_paketlar.view.*
import kotlinx.android.synthetic.main.fragment_u_s_s_d.view.*

class SMSPaketlarFragment : Fragment() {
    private val listFragments : MutableList<Fragment> = ArrayList()
    private val titles : MutableList<String> =ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        listFragments.apply {
            add(KunlikSMSlarFragment())
            add(HaftalikSMSlarFragment())
            add(OylikSMSlarFragment())
            add(XalqaroSMSlarFragment())
        }
        titles.apply {
            add("Kunlik")
            add("Haftalik")
            add("Oylik")
            add("Xalqaro")
        }
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_s_m_s_paketlar, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val tabAdapter= TabAdapter(fragmentManager!!,listFragments,titles)
        view_pager_sms.adapter=tabAdapter
        sms_tab_layout.setupWithViewPager(view_pager_sms)
        setMarginTabItems()
        /**<Action OnBack>*/
        view.btn_back_sms_paketlar.setOnClickListener {
            replaceFragment(HomePageFragment())
        }
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && event.action === KeyEvent.ACTION_UP) {
                replaceFragment(HomePageFragment())
                return@OnKeyListener true
            }
            false
        })
        /**<Action OnBack/>*/
        super.onViewCreated(view, savedInstanceState)
    }
    private fun replaceFragment(fragment: Fragment) {
        fragmentManager!!.beginTransaction().setCustomAnimations(
            R.anim.enter_from_right, R.anim.exit_to_left,
            R.anim.enter_from_right, R.anim.exit_to_left
        )
            .replace(R.id.frame_for_fragments, fragment)
            .commit()
    }
    fun setMarginTabItems(){
        for (i in 0 until sms_tab_layout.tabCount){
            val tabItem=(sms_tab_layout.getChildAt(0) as ViewGroup).getChildAt(i)
            val params=tabItem.layoutParams as ViewGroup.MarginLayoutParams
            params.setMargins(25,0,25,0)
            params.width=270
            params.height=100
        }
    }
}