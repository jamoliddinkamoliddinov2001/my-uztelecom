package com.example.myuztelecom.FragmentsAll.USSDFragments

import android.os.Bundle
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.concatexpandablerecyclerview.GroupItem
import com.example.concatexpandablerecyclerview.RecyclerViewConcatAdapter
import com.example.myuztelecom.FragmentsAll.HomePageFragments.HomePageFragment
import com.example.myuztelecom.R
import kotlinx.android.synthetic.main.fragment_u_s_s_d.*
import kotlinx.android.synthetic.main.fragment_u_s_s_d.view.*

@Suppress("DEPRECATED_IDENTITY_EQUALS")
class USSDFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_u_s_s_d, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val adapterList = arrayListOf<RecyclerViewConcatAdapter>()
        val groupList = createItems(40)
        for (item in groupList) {
            val adapter = RecyclerViewConcatAdapter(item, requireContext())
            adapterList.add(adapter)
        }
        val concatcConfig = ConcatAdapter.Config.Builder().setIsolateViewTypes(false).build()
        val adapter = ConcatAdapter(concatcConfig, adapterList)
        concat_rv_ussd.layoutManager = LinearLayoutManager(requireContext())
        concat_rv_ussd.adapter = adapter
        /**<Action OnBack>*/
        view.btn_back_usssd.setOnClickListener {
            replaceFragment(HomePageFragment())
        }
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && event.action === KeyEvent.ACTION_UP) {
                replaceFragment(HomePageFragment())
                return@OnKeyListener true
            }
            false
        })
        /**<Action OnBack/>*/
        super.onViewCreated(view, savedInstanceState)
    }

    private fun replaceFragment(fragment: Fragment) {
        fragmentManager!!.beginTransaction()
            .setCustomAnimations(
                R.anim.enter_from_left,
                R.anim.exit_to_right
            )
            .replace(R.id.frame_for_fragments, fragment)
            .commit()
    }

    fun createItems(numberOfHeads: Int): List<GroupItem> {
        val groupList = arrayListOf<GroupItem>()
        val titles = arrayListOf<String>(
            "Balans",
            "Men kimman",
            "Menga qo'ng'iroq qiling",
            "Xisobni to'ldirish",
            "Mobil Anons",
            "Play Mobile",
            "Ovozli Pochta",
            "Qo'llab yubor"
        )
        for (i in titles.indices) {
            val textCode= "*10$i#"
            val subText="Tekshirish"
            val header = GroupItem.HeaderItem(textCode, R.color.uzmobile, titles[i])
            val groupItem = GroupItem(header, GroupItem.SubItem(subText))
            groupList.add(groupItem)
        }
        return groupList
    }
}