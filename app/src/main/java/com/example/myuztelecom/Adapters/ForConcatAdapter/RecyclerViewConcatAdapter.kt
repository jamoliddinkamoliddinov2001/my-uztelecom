package com.example.concatexpandablerecyclerview

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myuztelecom.R
import de.hdodenhof.circleimageview.CircleImageView

@SuppressLint("StaticFieldLeak")
lateinit var mContext: Context

class RecyclerViewConcatAdapter(private val groupItem: GroupItem, val context: Context) :
    RecyclerView.Adapter<RecyclerViewConcatAdapter.ViewHolder>() {
    init {
        mContext = context
    }

    companion object {
        const val VIEW_HEADER = 0
        const val VIEW_SUB = 1
    }

    var isExpandable = false

    sealed class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        class HeaderViewHolder(headerItemView: View) : ViewHolder(headerItemView) {
            private val circleImage =
                headerItemView.findViewById<CircleImageView>(R.id.circle_image_concat)
            private val btnUpDown =
                headerItemView.findViewById<RelativeLayout>(R.id.item_up_down)
            private val tvCode = headerItemView.findViewById<TextView>(R.id.tv_code_concat)
            private val tvDesc = headerItemView.findViewById<TextView>(R.id.tv_desc_concat)
            private val tvTitle = headerItemView.findViewById<TextView>(R.id.tv_title_concat)

            fun onBind(headerItem: GroupItem.HeaderItem, onClickListener: View.OnClickListener) {
                circleImage.setImageResource(headerItem.imageRec)
                tvCode.text = headerItem.textCode
                tvTitle.text = headerItem.title
                btnUpDown.setOnClickListener { view ->
                    // isExpandable2 = !isExpandable2
                    onClickListener.onClick(view)
                }
            }
        }

        class SubViewHolder(subItemView: View) : ViewHolder(subItemView) {
            private val subTextView = subItemView.findViewById<Button>(R.id.sub_item_button)
            fun onBind(subItem: GroupItem.SubItem, onSubClicked: View.OnClickListener) {
                subTextView.text = subItem.textSub
                subTextView.setOnClickListener { view ->
                    onSubClicked.onClick(view)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            VIEW_HEADER -> {
                ViewHolder.HeaderViewHolder(
                    layoutInflater.inflate(
                        R.layout.concat_header_item,
                        parent,
                        false
                    )
                )
            }
            else -> {
                ViewHolder.SubViewHolder(
                    layoutInflater.inflate(
                        R.layout.concat_sub_item,
                        parent,
                        false
                    )
                )
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when (holder) {
            is ViewHolder.HeaderViewHolder -> holder.onBind(groupItem.header, onHeaderClicked())
            is ViewHolder.SubViewHolder -> holder.onBind(groupItem.subItem, onSubClicked(position))
        }
    }

    override fun getItemCount(): Int {
        return if (isExpandable) {
            1
        } else 1
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) VIEW_HEADER else VIEW_SUB
    }


    private fun onHeaderClicked() = View.OnClickListener { view ->
        view as RelativeLayout
        isExpandable = !isExpandable
        //   ForCheckResults.setIsExpandable(isExpandable)
        if (isExpandable) {
            notifyItemChanged(0)
            notifyItemRangeInserted(1, 1)
        } else {
            notifyItemChanged(0)
            notifyItemRangeRemoved(0, 1)
        }
        //      Log.d("EXPAND", "clickda $isExpandable2")
    }

    private fun onSubClicked(position: Int) = View.OnClickListener {
        if (isExpandable) {
            mContext.startActivity(
                Intent(
                    Intent.ACTION_DIAL,
                    Uri.fromParts("tel", "*10$position#", null)
                )
            )
        }
    }
}