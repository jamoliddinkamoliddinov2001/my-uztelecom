package com.example.navigationdrwercustom

import android.view.ViewGroup
import android.view.ViewParent
import androidx.recyclerview.widget.RecyclerView

abstract class DrawerItem<T:DrawerAdapter.viewHolder> {
    var ischecked=false
    protected set
    abstract fun createViewHolder(parent: ViewGroup?):T
    abstract fun bindViewHolder(holder: T?)
    fun setItemChecked(isChecked:Boolean):DrawerItem<T>{
        this.ischecked=isChecked
        return this
    }
    open val isSelecteble:Boolean
    get() = true
}