package com.example.concatexpandablerecyclerview

class GroupItem(val header: HeaderItem,val subItem:SubItem) {
    class HeaderItem(val textCode:String,val imageRec:Int,val title:String)
    class SubItem(val textSub:String)
}
