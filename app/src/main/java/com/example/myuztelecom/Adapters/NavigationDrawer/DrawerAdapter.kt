package com.example.navigationdrwercustom

import android.util.SparseArray
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myuztelecom.FragmentsAll.HomePageFragments.HomePageFragment

class DrawerAdapter(protected val items: List<DrawerItem<DrawerAdapter.viewHolder>>) :
    RecyclerView.Adapter<DrawerAdapter.viewHolder>() {

    private  var viewTypes: MutableMap<Class<out DrawerItem<*>>, Int?> = HashMap()
    private  var holderFactories: SparseArray<DrawerItem<*>> = SparseArray()
    private var listener: OnItemSelectedListener? = null

    init {
        processViewTypes()
    }

    private fun processViewTypes() {
        var type = 0
        for (item in items) {
            if (!viewTypes.containsKey(item.javaClass)) {
                viewTypes[item.javaClass] = type
                holderFactories.put(type, item)
                type++
            }
        }
    }

    abstract class viewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        var drawerAdapter: DrawerAdapter? = null
        override fun onClick(p0: View?) {
            drawerAdapter!!.setSelected(adapterPosition)
        }
        init {
            itemView.setOnClickListener(this)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        val holder = holderFactories[viewType].createViewHolder(parent)
        holder?.drawerAdapter = this
        return holder!!
    }

    override fun onBindViewHolder(holder: viewHolder, position: Int) {
        items[position].bindViewHolder(holder)
    }

    override fun getItemCount(): Int {
        return items.size
    }
fun setSelected(position: Int){
        val newChecked=items[position]
    if (!newChecked.isSelecteble){
        return
    }
    for (i in items.indices){
        val item=items[i]
        if (item.ischecked){
            item.setItemChecked(false)
            notifyItemChanged(i)
            break
        }
    }
    newChecked.setItemChecked(true)
    notifyItemChanged(position)
    if (listener!=null){
        listener!!.onNavDrawerItemSelected(position)
    }
}
    fun setListener(listener: HomePageFragment){
        this.listener=listener
    }
    interface OnItemSelectedListener {
        fun onNavDrawerItemSelected(position: Int) {

        }
    }
}