package com.example.myuztelecom.Adapters.ForTabLayout

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.myuztelecom.FragmentsAll.SMSFragments.ForTabs.HaftalikSMSlarFragment
import com.example.myuztelecom.FragmentsAll.SMSFragments.ForTabs.KunlikSMSlarFragment
import com.example.myuztelecom.FragmentsAll.SMSFragments.ForTabs.OylikSMSlarFragment
import com.example.myuztelecom.FragmentsAll.SMSFragments.ForTabs.XalqaroSMSlarFragment
import kotlin.collections.ArrayList

class TabAdapter(fragmentManager: FragmentManager,val listFragments:MutableList<Fragment>,val listTitles:MutableList<String>):FragmentStatePagerAdapter(fragmentManager) {
    override fun getCount(): Int {
        return listFragments.size
    }
    override fun getItem(position: Int): Fragment {
        val fragment1:Fragment=listFragments[0]
        val fragment2:Fragment=listFragments[1]
        val fragment3:Fragment=listFragments[2]
        val fragment4:Fragment=listFragments[3]
        when(position){
            0-> return fragment1
            1-> return fragment2
            2-> return fragment3
            3-> return fragment4
        }
        return KunlikSMSlarFragment()
    }
    override fun getPageTitle(position: Int): CharSequence {
        return listTitles[position]
    }
}