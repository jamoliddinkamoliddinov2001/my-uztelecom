package com.example.navigationdrwercustom

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myuztelecom.R

class SimpleItem(private val icon: Drawable, private val title: String) :
    DrawerItem<SimpleItem.ViewHolder>() {
    private var selectedItemIconTint=0
    private var selectedItemTextTint=0
    private var normalItemIconTint=0
    private var normalItemTextTint=0

    class ViewHolder(itemView: View) : DrawerAdapter.viewHolder(itemView) {
        val icon: ImageView
        val title: TextView

        init {
            icon = itemView.findViewById(R.id.icon)
            title = itemView.findViewById(R.id.title)
        }
    }

    override fun createViewHolder(parent: ViewGroup?): ViewHolder {
        val inflater = LayoutInflater.from(parent?.context)
        val view = inflater.inflate(R.layout.nav_drawer_item_option, parent, false)
        return ViewHolder(view)
    }

    override fun bindViewHolder(holder: ViewHolder?) {
        holder!!.icon.setImageDrawable(icon)
        holder.title.text=title
        holder.title.setTextColor(if (ischecked)selectedItemTextTint else normalItemTextTint)
        holder.icon.setColorFilter(if (ischecked) selectedItemIconTint else normalItemIconTint)
    }

    fun withSelectedIconTint(selectedItemIconTint: Int): SimpleItem {
        this.selectedItemIconTint=selectedItemIconTint
        return this
    }
    fun withSelectedTextTint(selectedItemTextTint: Int): SimpleItem {
        this.selectedItemTextTint=selectedItemTextTint
        return this
    }
    fun withNormalIconTint(normalIconItemTint: Int): SimpleItem {
        this.normalItemIconTint=normalIconItemTint
        return this
    }
    fun withNormalTextTint(normalItemTextTint: Int): SimpleItem {
        this.normalItemTextTint=normalItemTextTint
        return this
    }
}