package com.example.myuztelecom.Adapters.ForViewPagerFlipper

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myuztelecom.R
import kotlinx.android.synthetic.main.flipper_layout.view.*

class IntroAdapter(private val introList: List<IntroItem>) :
    RecyclerView.Adapter<IntroAdapter.IntroViewHolder>() {
    lateinit var listener: OnItemClickedListener
    inner class IntroViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {
        init {
            itemview.setOnClickListener { 
                if (adapterPosition!=RecyclerView.NO_POSITION){
                    listener.onItemClicked(adapterPosition)
                }
            }
        }
        fun onBind(item: IntroItem) {
            itemView.tv_tarif_name.text = item.tarifName
            itemView.tv_min.text = item.min.toString()
            itemView.tv_mb.text = item.mb.toString()
            itemView.tv_sms.text = item.sms.toString()
            itemView.tv_tolov.text = item.tolov.toString()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IntroViewHolder {
        return IntroViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.flipper_layout, parent, false)
        )
    }

    override fun onBindViewHolder(holder: IntroViewHolder, position: Int) {
        holder.onBind(introList[position])
    }

    override fun getItemCount(): Int {
        return introList.size
    }
    fun setOnItemClicked(listener: OnItemClickedListener){
        this.listener =  listener
    }
}