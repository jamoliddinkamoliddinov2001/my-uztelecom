package com.example.myuztelecom.Adapters.ForViewPagerFlipper

interface OnItemClickedListener {
    fun onItemClicked(position: Int)
}